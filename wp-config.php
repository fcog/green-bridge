<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'greenbridge_wp');

/** MySQL database username */
define('DB_USER', 'greenbridge');

/** MySQL database password */
define('DB_PASSWORD', 'ksJd7*2H77%');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Szp?Z de]f>U(Kh=;l=]c`-;Dfm@`):Z|i]I}C7a|5e4?CX%2%YkV[Z}`I!2FpHq');
define('SECURE_AUTH_KEY',  'el3u9G|TH=Fn@%!(5>4I+5$GHVp>][=2ls9v1$~Gd?uYk,.@T~Sgl/R-tO8X&MI@');
define('LOGGED_IN_KEY',    '+GYZ0xLfk< b =[N#> lh1MG-VoF|a/Mv/e0l#:@Zxh=,26Wr33yR1mZr%C6;NDG');
define('NONCE_KEY',        'Z-VXq~xQMRgqlqJy[=L?Ju$~PA&xkSi5BYkLw<N| 19o,GL~_Aiw)/Ve)L$C4Xg]');
define('AUTH_SALT',        'D=&^5KIPAgu.yxdCJOZf:q/6eYX/e>k+c`MUWKN^<t((9[oPDHq9do};1 k}Iv|>');
define('SECURE_AUTH_SALT', '_SLTAo:QyBP5avvTsG,QTxa763{MijFt-q+huYP}c J9ou{Q>N!Rt`@v&TQ72X@R');
define('LOGGED_IN_SALT',   '>1wT7&$dZCW(q]w5dm}X|duWau]Whks=RAxCofRnW<fo#*^>og5Z*(O-ohHftQ8;');
define('NONCE_SALT',       '5GI(G%C%JpV]d@hMJk<M<}D{$M|_ys[eI42VvYQ#g0xG3b~,x;/sR:>w1UImo:?s');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
