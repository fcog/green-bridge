<?php get_header(); ?>

    <div id="primary" class="content-area">

        <?php if (get_post_type() != 'programmes' && get_post_type() != 'page'): ?>
            <div id="content" class="site-content has-sidebar" role="main">
        <?php else: ?>
            <div id="content" class="site-content" role="main">
        <?php endif ?>

        <?php if ( have_posts() ) : ?>
            <?php /* The loop */ ?>
            <?php while ( have_posts() ) : the_post(); ?>
                <?php get_template_part( 'content', get_post_format() ); ?>
            <?php endwhile; ?>

        <?php else : ?>
            <?php get_template_part( 'home', '' ); ?>
        <?php endif; ?>

        </div><!-- #content -->
        <?php if (in_category('News')) get_sidebar('news'); ?>
        <?php if (in_category('Projects')) get_sidebar('projects'); ?>
    </div><!-- #primary -->
<?php get_footer(); ?>