<?php
/*
Template Name: Central Asia Grows Green
*/
?>

<?php get_header(); ?>

<div id="primary" class="container_24">
  <div id="content" class="site-content" role="main">

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <header class="entry-header">
      <h1><?php echo the_title() ?></h1>
    </header>  
    <div class="entry-content">
      <section class="sidebar grid_8">
        <ul class="side-menu">
          <?php if (qtrans_getLanguage() == 'en'): ?>
          <li><a href="#intro">Introduction</a></li>
          <li><a href="#panel1">Panel 1: Natural Resource Management for Sustainable Development</a></li>
          <li><a href="#panel2">Panel 2: Resilience in the face of Climate Change</a></li>
          <li><a href="#panel3">Panel 3: Moving towards a green economy</a></li>
          <?php else: ?>
          <li><a href="#panel1">Панельное заседание 1 «Устойчивое использование природных ресурсов»</a></li>
          <li><a href="#panel2">Панельное заседание 2 Устойчивость к изменению климата</a></li>
          <li><a href="#panel3">Панельное заседание 3 «Переход к  зеленой экономике»</a></li>        
          <?php endif ?>
        </ul>
      </section>
      <div class="white-content grid_16 alpha omega" >
        <p><?php the_content(); ?></p>
      </div>
  </div>

  <?php endwhile; endif ?>

</div> 
</div>  

<?php get_footer(); ?>
