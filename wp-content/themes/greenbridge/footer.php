    <footer>
        <div id="sitemap" class="container_24">
            <div class="grid_12">
                <div id="logo-footer" >
                    <img src="<?php echo get_template_directory_uri(); ?>/photos/logo.png">
                </div>
                <div id="description">
<?php if (qtrans_getLanguage() == 'en'): ?>
The Programme intends to foster green economic growth in Central Asia
and wider region through technology transfer, knowledge sharing, and
financial support for the implementation of investment.
<?php else: ?>
Миссия ППЗМ сформулирована следующим образом:
«Управление «зеленым» экономическим ростом в Центральной Азии через международное сотрудничество и содействие при передаче технологий, обмене знаниями и финансовой поддержке».
<?php endif ?>
</div>
            </div>
            <?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_class' => 'grid_12' ) ); ?>
        </div>
        <div id="copyright">
            © Copyright 2013 greenbridge.kz <?php if (qtrans_getLanguage() == 'en'): ?>All Rights Reserved.<?php else: ?>Все права защищены.<?php endif ?>
        </div>
    </footer>
</div>
<?php wp_footer(); ?>
</body>
</html>