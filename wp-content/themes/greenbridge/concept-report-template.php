<?php
/*
Template Name: Concept Report
*/
?>

<?php get_header(); ?>

<div id="primary" class="container_24">
  <div id="content" class="site-content" role="main">

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <header class="entry-header">
      <h1><?php echo the_title() ?></h1>
    </header>  
    <div class="entry-content">
    <section class="sidebar grid_8">
      <ul class="side-menu2">
        <?php if (qtrans_getLanguage() == 'en'): ?>
        <li>
          1. <a href="#link1">Introduction</a>
          <ul>
            <li>1.1 <a href="#link11">Green Economic Growth - accelerating and sustaining growth</a></li>
            <li>1.2 <a href="#link12">Rationale for a green growth programme</a></li>
            <li>1.3 <a href="#link13">Overcoming barriers to investment in green growth</a></li>
          </ul>
        </li>
        <li>
          2. <a href="#link2">The Green Bridge Partnership Programme</a>
          <ul>
            <li>2.1 <a href="#link21">Mission</a></li>
            <li>2.2 <a href="#link22">GBPP goals and objectives</a></li>
            <li>2.3 <a href="#link23">Governing principles</a></li>
            <li>2.4 <a href="#link24">Key thematic areas</a></li>
          </ul>
        </li>
        <li>
          3. <a href="#link3">GBPP structure and governance</a>
          <ul>
            <li>3.1 <a href="#link31">Principle operating entities</a></li>
            <li>3.2 <a href="#link32">Governance</a></li>
          </ul>          
        </li>
        <li>
          4. <a href="#link4">Green Bridge Institute</a>
          <ul>
            <li>4.1 <a href="#link41">Overview</a></li>
            <li>4.2 <a href="#link42">Research framework</a></li>
            <li>4.3 <a href="#link43">Technulogy incubator</a></li>
          </ul>          
        </li>
        <li>
          5. <a href="#link5">Green Bridge Facility</a>
          <ul>
            <li>5.1 <a href="#link51">Basic overview</a></li>
            <li>5.2 <a href="#link52">Technical Assistance (grant financed)</a></li>
            <li>5.3 <a href="#link53">Financing projects</a></li>
          </ul>          
        </li>
        <li>
          6. <a href="#link6">GBPP and EXPO 2017</a>
          <ul>
            <li>6.1 <a href="#link61">Alignment of GBPP and EXPO </a></li>
            <li>6.2 <a href="#link62">How to further align GBPP’s activities with EXPO </a></li>
          </ul>          
        </li>
        <?php else: ?>
        <li>
          1. <a href="#link1">Введение</a>
          <ul>
            <li>1.1 <a href="#link11">«Зеленый» экономический рост – прогрессивный и устойчивый рост</a></li>
            <li>1.2 <a href="#link12">Обоснование программы «зеленого» роста</a></li>
            <li>1.3 <a href="#link13">Преодоление барьеров для инвестирования в «зеленый» рост</a></li>
          </ul>
        </li>
        <li>
          2. <a href="#link2">Программа партнерства «Зеленый мост»</a>
          <ul>
            <li>2.1 <a href="#link21">Миссия</a></li>
            <li>2.2 <a href="#link22">Цели и задачи</a></li>
            <li>2.3 <a href="#link23">Руководящие принципы</a></li>
            <li>2.4 <a href="#link24">Ключевые тематические области</a></li>
          </ul>
        </li>
        <li>
          3. <a href="#link3">Структура и руководство ППЗМ</a>
          <ul>
            <li>3.1 <a href="#link31">Главные функционирующие организации</a></li>
            <li>3.2 <a href="#link32">Руководство</a></li>
          </ul>          
        </li>
        <li>
          4. <a href="#link4">Институт «Зеленый мост»</a>
          <ul>
            <li>4.1 <a href="#link41">Обзор</a></li>
            <li>4.2 <a href="#link42">Тематика исследований</a></li>
            <li>4.3 <a href="#link43">Технологический инкубатор</a></li>
          </ul>          
        </li>
        <li>
          5. <a href="#link5">Учреждение «Зеленый мост»</a>
          <ul>
            <li>5.1 <a href="#link51">Общая характеристика</a></li>
            <li>5.2 <a href="#link52">Техническая помощь</a></li>
            <li>5.3 <a href="#link53">Финансирование проектов</a></li>
          </ul>          
        </li>
        <li>
          6. <a href="#link6">ППЗМ и EXPO 2017</a>
          <ul>
            <li>6.1 <a href="#link61">Общие цели ППЗМ и EXPO 2017</a></li>
            <li>6.2 <a href="#link62">Как получить синергетический эффект от деятельности ППЗМ и проведения EXPO 2017</a></li>
          </ul>          
        </li>        
        <?php endif ?>        
      </ul>
    </section>
    <div class="white-content grid_16 alpha omega" >
      <p><?php the_content(); ?></p>
    </div>
  </div>

  <?php endwhile; endif ?>
  </div>
</div>  

<?php get_footer(); ?>
