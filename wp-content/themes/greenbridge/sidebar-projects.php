	<?php if ( is_active_sidebar( 'sidebar-projects' ) ) : ?>
		<div id="secondary" class="widget-area" role="complementary">
			<?php dynamic_sidebar( 'sidebar-projects' ); ?>
		</div><!-- #secondary -->
	<?php endif; ?>