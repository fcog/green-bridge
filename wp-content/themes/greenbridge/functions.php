<?php

function greenbridge_setup() {
	register_nav_menu( 'primary', __( 'Navigation Menu', 'greenbridge' ) );
	register_nav_menu( 'footer', __( 'Footer Menu', 'greenbridge' ) );
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 292, 140, true );
}
add_action( 'after_setup_theme', 'greenbridge_setup' );

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function greenbridge_widgets_init() {

/************* HEADER *********************/
    register_sidebar( array(
      'id'            => 'homepage-social-buttons',
      'name'          => __( 'Homepage - Social buttons', 'greenbridge' ),
      'before_widget'  => '',                  
    ) );

    register_sidebar( array(
      'id'            => 'homepage-languages',
      'name'          => __( 'Homepage - Languages', 'greenbridge' ),
      'before_widget'  => '',                  
    ) );

    register_sidebar( array(
      'id'            => 'homepage-projects',
      'name'          => __( 'Homepage - Projects', 'greenbridge' ),
      'before_widget'  => '',                  
    ) );  

    register_sidebar( array(
      'id'            => 'sidebar-news',
      'name'          => __( 'Sidebar - News', 'greenbridge' ),  
      'before_widget'  => '',                  
    ) );  

    register_sidebar( array(
      'id'            => 'sidebar-projects',
      'name'          => __( 'Sidebar - Projects', 'greenbridge' ),  
      'before_widget'  => '',                  
    ) );  

}
add_action( 'widgets_init', 'greenbridge_widgets_init' );

/************* CUSTOM WIDGETS *****************/

class ProjectLinkWidget extends WP_Widget {
         public function __construct() {
               parent::WP_Widget(false,'Homepage Project Link','description=Homepage Project Links');
        }

        public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'link' => '' ) );
			    $title = $instance['title'];
			    $link = $instance['link'];
			?>
			  <p><label for="<?php echo $this->get_field_id('title'); ?>">Title: </label><input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>"/></p>

			  <p><label for="<?php echo $this->get_field_id('link'); ?>">Link: </label><input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="text" value="<?php echo attribute_escape($link); ?>" /></p>
			<?php
        }

        public function update( $new_instance, $old_instance ) {
		    $instance = $old_instance;
		    $instance['title'] = $new_instance['title'];
		    $instance['link'] = $new_instance['link'];
		    return $instance;
        }

        public function widget( $args, $instance ) {
		    extract($args, EXTR_SKIP);
		 
		    echo $before_widget;
		    $title = empty($instance['title']) ? '' : apply_filters('widget_title', $instance['title']);
		    $link = empty($instance['link']) ? '' : apply_filters('widget_title', $instance['link']);

		    echo '<div class="banner project'.$this->number.'">';

		    if (!empty($title))
		      echo "<h2>".$title."</h2>";

		    if (!empty($link)){
		      if (qtrans_getLanguage() == 'en'){
		      	$text = 'View Project';
		      }
		      else{
		      	$text = 'О проекте';
		      }
		      
		      echo '<a href="'.$link.'">'.$text.'</a>';
		    }

		  	echo "</div>";

		    echo $after_widget;
        }

}
register_widget( 'ProjectLinkWidget' );


/************* SOCIAL LINKS *****************/

class SocialLinkWidget extends WP_Widget {
         public function __construct() {
               parent::WP_Widget(false,'Social Link','description=Social Link');
        }

        public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, array( 'class' => '', 'link' => '' ) );
			    $class = $instance['class'];
			    $link = $instance['link'];
			?>
			  <p><label for="<?php echo $this->get_field_id('class'); ?>">CSS Class: </label><input class="widefat" id="<?php echo $this->get_field_id('class'); ?>" name="<?php echo $this->get_field_name('class'); ?>" type="text" value="<?php echo attribute_escape($class); ?>"/></p>

			  <p><label for="<?php echo $this->get_field_id('link'); ?>">Link: </label><input class="widefat" id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" type="text" value="<?php echo attribute_escape($link); ?>" /></p>
			<?php
        }

        public function update( $new_instance, $old_instance ) {
		    $instance = $old_instance;
		    $instance['class'] = $new_instance['class'];
		    $instance['link'] = $new_instance['link'];
		    return $instance;
        }

        public function widget( $args, $instance ) {
		    extract($args, EXTR_SKIP);
		 
		    echo $before_widget;
		    $class = empty($instance['class']) ? '' : apply_filters('widget_title', $instance['class']);
		    $link = empty($instance['link']) ? '' : apply_filters('widget_title', $instance['link']);

		    if (!empty($link))
		      echo "<li class='".$class."'><a href='".$link."' target='_blank'></a></li>";

		    echo $after_widget;
        }

}
register_widget( 'SocialLinkWidget' );

/************* HELPER FUNCTIONS *****************/

function html_widget_title( $title ) {
	//HTML tag opening/closing brackets
	$title = str_replace( '[', '<', $title );
	$title = str_replace( ']', '/>', $title );

	return $title;
}
add_filter( 'widget_title', 'html_widget_title' );