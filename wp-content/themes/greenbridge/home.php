<?php
/*
Template Name: Home
*/
?>

<?php get_header(); ?>
    <section id="main-banner">
        <div id="silder" class="container_24">
<!--             <div id="text" class="grid_11">
                <h2>The Green Bridge <br /> Conference 2013</h2>
                <p>The Green Bridge Conference 2013 will also represent the launch of The Green Bridge Partnership Program, an initiative intended to bring together the countries of Central Asia with 
                </p>
                <a href="#">Read More</a>
            </div>
            <div id="slider-image" class="grid_13">
                <img src="<?php echo get_template_directory_uri(); ?>/photos/img-slider1.png">   
            </div> -->
            <?php echo do_shortcode('[nggallery id=4 template="sliderview"]'); ?>
        </div>
    </section>
    <section id="news">
        <div class="container_24">
	        <?php
            $args = array(
                'posts_per_page'   => 3,
                'category'         => 4,
            );

            $posts_array = get_posts( $args );

	        foreach ( $posts_array as $post ): 
            ?>
	    
		        <article>
		        	<time class="date"><?php the_time('m-d-Y'); ?></time>
		            <h3><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3>
		            <?php if (has_post_thumbnail()): ?><div class="img-news"><?php the_post_thumbnail(); ?></div><?php endif ?>
		            <?php the_excerpt(); ?>
		            <a href="<?php echo get_permalink(); ?>" class="read-more"><?php if (qtrans_getLanguage() == 'en'): ?>Full Article<?php else: ?>Ссылки<?php endif ?></a>
		        </article>

            <?php endforeach; ?>

        </div>
    </section>
    <section id="projects">
            <?php
            if (is_active_sidebar('homepage-projects')) :
                dynamic_sidebar('homepage-projects');
            endif;
            ?>        
    </section>
    <section id="links">
        <div class="container_24">
            <div class="label"><?php if (qtrans_getLanguage() == 'en'): ?>Links<?php else: ?>Ссылки<?php endif ?></div>
            <?php echo do_shortcode('[tiny-carousel-slider id="1"]') ?>
        </div>
    </section>

<?php get_footer(); ?>